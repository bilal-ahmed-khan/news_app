import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>with SingleTickerProviderStateMixin {
  double _scale;
  AnimationController _controller;
  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 500,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
      setState(() {});
    });
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;
    final Shader linearGradient = LinearGradient(
      colors: <Color>[Color(0xff002f71),Color(0xffb91017)],
    ).createShader(new Rect.fromLTWH(95.0, 80.0, 200.0, 70.0));
    return Scaffold(
resizeToAvoidBottomPadding: true,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 250.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.white60,
              child: Stack(

                children: [
                  Container(
                    height: 180.0,
                    width: MediaQuery.of(context).size.width,
                    // color: Colors.blue,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/icons/background.jpg'),
                        fit: BoxFit.cover
                      )
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: Container(
                        height: 100.0,
                        width: 100.0,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                            color: Colors.white,
                            shape: BoxShape.circle
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Container(
                        height: 90.0,
                        width: 110.0,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage('assets/icons/profile_pic.jpg'),
                                fit: BoxFit.contain
                            )
                        ),
                      ),
                    ),
                  ),

                ],)
            ),
            Center(
                child: Text(
                  'René Mirck',
                  textScaleFactor: 1.5,
                  style: new TextStyle(
                      // fontSize: 60.0,
                      fontWeight: FontWeight.bold,
                      foreground: new Paint()..shader = linearGradient),
                )),

            Divider(
              height: 20,
              endIndent: 20.0,
              indent: 20.0,
              thickness: 2,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                    Text('Wonen',
                      textScaleFactor: 0.8,
                      style: TextStyle(
                      color: Color(0xff002f71),
                        fontWeight: FontWeight.bold
                    ),),
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xff002f71),
                        borderRadius: BorderRadius.circular(5)
                      ),
                      // width: 100.0,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                        child: Center(
                          child: Text('Friesland',
                            textScaleFactor: 0.65,
                            style: TextStyle(
                            color: Colors.white60,
                              fontWeight: FontWeight.bold
                          ),),
                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
                  Container(
                      decoration: BoxDecoration(
                        color: Color(0xff002f71),
                        borderRadius: BorderRadius.circular(5)
                      ),
                      // width: 100.0,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                        child: Center(
                          child: Text('Leeuwarden',
                            textScaleFactor: 0.65,
                            style: TextStyle(
                            color: Colors.white60,
                              fontWeight: FontWeight.bold
                          ),),
                        ),
                      ),
                    ),
                ],
              ),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Werk',
                    textScaleFactor: 0.8,
                    style: TextStyle(
                        color: Color(0xff002f71),
                        fontWeight: FontWeight.bold
                    ),),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xff002f71),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                          child: Center(
                            child: Text('Director Wappiez',
                              textScaleFactor: 0.65,
                              style: TextStyle(
                              color: Colors.white60,
                                fontWeight: FontWeight.bold
                            ),),
                          ),
                        ),
                      ),
                      SizedBox(width: 15,),
                    Container(
                        decoration: BoxDecoration(
                          color: Color(0xff002f71),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        width: MediaQuery.of(context).size.width * 0.44,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                          child: Center(
                            child: Text('Was eignaar the reted family',
                              textScaleFactor: 0.65,
                              style: TextStyle(
                              color: Colors.white60,
                                fontWeight: FontWeight.bold
                            ),),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Websites',
                    textScaleFactor: 0.8,
                    style: TextStyle(
                        color: Color(0xff002f71),
                        fontWeight: FontWeight.bold
                    ),),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xff002f71),
                        borderRadius: BorderRadius.circular(5)
                      ),
                      // width: 100.0,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                        child: Center(
                          child: Text('www.Wappiez.com',
                            textScaleFactor: 0.65,
                            style: TextStyle(
                            color: Colors.white60,
                              fontWeight: FontWeight.bold
                          ),),
                        ),
                      ),
                    ),

                ],
              ),
            ),SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Gelooft',
                    textScaleFactor: 0.8,
                    style: TextStyle(
                        color: Color(0xff002f71),
                        fontWeight: FontWeight.bold
                    ),),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xff002f71),
                        borderRadius: BorderRadius.circular(5)
                    ),
                    width: MediaQuery.of(context).size.width * 0.44,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                      child: Center(
                        child: Text('Coronapandemie is een hoax',
                          textScaleFactor: 0.65,
                          style: TextStyle(
                              color: Colors.white60,
                              fontWeight: FontWeight.bold
                          ),),
                      ),
                    ),
                  ),
                  SizedBox(width: 15,),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xff002f71),
                        borderRadius: BorderRadius.circular(5)
                    ),
                    width: MediaQuery.of(context).size.width * 0.38,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8),
                      child: Center(
                        child: Text('MH17 is een false flag',
                          textScaleFactor: 0.65,
                          style: TextStyle(
                              color: Colors.white60,
                              fontWeight: FontWeight.bold
                          ),),
                      ),
                    ),
                  ),

                ],
              ),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Center(
                    child: Text('Mijn Wappiez',
                      textScaleFactor: 0.8,
                      style: TextStyle(
                          color:Color(0xff002f71),
                          fontWeight: FontWeight.bold
                      ),),
                  ),
                  SizedBox(width: 15,),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[400],
                        borderRadius: BorderRadius.circular(15)
                    ),
                    width: 100.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Center(
                            child: Text('Zoeken',
                              textScaleFactor: 0.65,
                              style: TextStyle(
                                  color: Colors.white60,
                                  fontWeight: FontWeight.bold
                              ),),
                          ),
                          Icon(Icons.search,color: Colors.white,size: 15,)
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 5),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [

                    Container(
                      height: 90.0,
                      width: 110.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/icons/Wappie-1-Lange-Frans.jpg'),
                              fit: BoxFit.cover
                          )
                      ),
                    ),
                    Container(
                      height: 90.0,
                      width: 110.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/icons/siskita.jpeg'),
                              fit: BoxFit.cover
                          )
                      ),
                    ),
                    Container(
                      height: 90.0,
                      width: 110.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/icons/Wappie-3-Robert-Jensen.jpg'),
                              fit: BoxFit.cover
                          )
                      ),
                    ), Container(
                      height: 90.0,
                      width: 110.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/icons/tisjeboy.jpeg'),
                              fit: BoxFit.cover
                          )
                      ),
                    ),

                  ],
                ),
              ),
            ),
            SizedBox(height: 15,),
        GestureDetector(
          onTapDown: _tapDown,
          onTapUp: _tapUp,
          child: Transform.scale(
            scale: _scale,

             child: _animatedButton(),
          ),
        ),

            Divider(
              height: 50 ,
              endIndent: 0.0,
              indent: 0.0,
              thickness: 15,
            ),

            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5)
              ),
              // height: 100.0,
              width: MediaQuery.of(context).size.width ,

              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Row(
                        children: [

                          Text('Mijn Derichten'),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Row(
                        children: [

                          Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/icons/profile_pic.jpg'),
                                    fit: BoxFit.cover
                                ),
                                shape: BoxShape.circle
                            ),
                            height: 35.0,
                            width: 35.0,

                          ),
                          SizedBox(width: 15,),

                          Expanded(

                            child: Container(
                              height: 35,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  border: Border.all(color: Colors.black,width: 0.5)
                              ),
                              child: Center(
                                child: TextField(
                                  style: TextStyle(fontSize: 11),
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Zing, vecht, huil, bid en speek',
                                      contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                                  ),
                                ),
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 60,top: 15),
                      child: Row(
                          children: [
                            Icon(Icons.camera_enhance_outlined,size: 18,),
                            SizedBox(width: 8,),
                            Icon(Icons.emoji_emotions_outlined,size: 18,),
                            SizedBox(width: 8,),
                            Text("T",textScaleFactor: 1.1,)

                          ]),
                    )
                  ],
                ),
              ),
            ),
            Divider(
              height: 40,
              endIndent: 0.0,
              indent: 0.0,
              thickness: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5)
                ),
                // height: 100.0,
                width: MediaQuery.of(context).size.width,

                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          // Container(
                          //   decoration: BoxDecoration(
                          //       color: Colors.blue,
                          //       shape: BoxShape.circle
                          //   ),
                          //   height: 35.0,
                          //   width: 35.0,
                          //
                          // ),
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/icons/profile_pic.jpg'),
                                        fit: BoxFit.cover
                                    ),
                                    shape: BoxShape.circle
                                ),
                                height: 35.0,
                                width: 35.0,

                              ),
                              SizedBox(width: 5,),
                              Text('René Mirck'),

                            ],
                          ),Row(
                            children: [
                              Text('3u'),
                              SizedBox(width: 5,),
                              Icon(Icons.more_vert_rounded),
                            ],
                          ),


                        ],
                      ),
                      Divider(
                        height: 25,
                        indent: 10,
                        endIndent: 10,
                        thickness: 1,
                      ),

                      Container(
                        child: Text('Lorem Ipsum is simply dummy text of the printing and typesetting industry.',textScaleFactor: 0.7,),
                      ),
                      SizedBox(height: 10,),

                      ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: GestureDetector(
                          onTap: (){
                            Navigator.of(
                                context)
                                .push(
                              PageRouteBuilder(
                                transitionDuration:
                                Duration(milliseconds: 500),
                                pageBuilder: (BuildContext context,
                                    Animation<double>
                                    animation,
                                    Animation<double>
                                    secondaryAnimation) {
                                  return GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Image(
                                        image: AssetImage('assets/icons/back2.jpeg'),
                                        fit: BoxFit.fill,
                                      ));
                                },
                                transitionsBuilder: (BuildContext context,
                                    Animation<double>
                                    animation,
                                    Animation<double>
                                    secondaryAnimation,
                                    Widget
                                    child) {
                                  return Align(
                                    child:
                                    FadeTransition(
                                      opacity: animation,
                                      child: child,
                                    ),
                                  );
                                },
                              ),
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/icons/back2.jpeg'),
                                  fit: BoxFit.cover
                              ),
                              // shape: BoxShape.circle
                            ),
                            width: 400.0,
                            height: 200.0,

                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Image(image: AssetImage('assets/icons/Frame.png'),width: 18,),

                              Text(' 99x Wappie',textScaleFactor: 0.7,style: TextStyle(
                                  color: Colors.grey
                              ),)
                            ],),
                            GestureDetector(
                              onTap: (){
                                bottomSheet();
                              },
                              child: Row(children: [
                                Image(image: AssetImage('assets/icons/comment.png'),width: 18,color: Color(0xff002f71),),
                                Text(' 48x Bestproken',textScaleFactor: 0.7,
                                  style: TextStyle(
                                      color: Colors.grey
                                  ),
                                )
                              ],),
                            ),
                            Row(children: [
                              Image(image: AssetImage('assets/icons/share.png'),width: 18,),
                              Text(' 48x Geedeeid',textScaleFactor: 0.7,style: TextStyle(
                                  color: Colors.grey
                              ),)
                            ],),
                          ],
                        ),
                      ),
                      // Row(
                      //   children: [
                      //
                      //     Container(
                      //       decoration: BoxDecoration(
                      //           image: DecorationImage(
                      //               image: AssetImage('assets/icons/profile_pic.jpg'),
                      //               fit: BoxFit.cover
                      //           ),
                      //           shape: BoxShape.circle
                      //       ),
                      //       height: 35.0,
                      //       width: 35.0,
                      //
                      //     ),
                      //     SizedBox(width: 15,),
                      //
                      //     Expanded(
                      //
                      //       child: Container(
                      //         height: 35,
                      //         decoration: BoxDecoration(
                      //             borderRadius: BorderRadius.circular(25),
                      //             border: Border.all(color: Colors.black,width: 0.5)
                      //         ),
                      //         child: Center(
                      //           child: TextField(
                      //             style: TextStyle(fontSize: 11),
                      //             decoration: InputDecoration(
                      //                 border: InputBorder.none,
                      //                 hintText: 'Zing, vecht, huil, bid en speek',
                      //
                      //                 contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                      //             ),
                      //           ),
                      //         ),
                      //       ),
                      //     )
                      //
                      //   ],
                      // ),
                    ],
                  ),
                ),
              ),
            ),

            SizedBox(height: 25,)
          ],
        ),
      ),
    );
  }
  Widget  _animatedButton() {
    return Container(
      height: 35,
      width: MediaQuery.of(context).size.width * 0.6,
      decoration: BoxDecoration(
        color: Colors.grey[400],
          borderRadius: BorderRadius.circular(100.0),
          boxShadow: [
            BoxShadow(
              color: Color(0x80000000),
              blurRadius: 12.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
          // gradient: LinearGradient(
          //   begin: Alignment.topLeft,
          //   end: Alignment.bottomRight,
          //   colors: [
          //     Color(0xff33ccff),
          //     Color(0xffff99cc),
          //   ],
          // )

      ),
      child: Center(
        child: Text(
          'Alle Wappiez zien',
          style: TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
      ),
    );
  }
  void _tapDown(TapDownDetails details) {
    _controller.forward();
  }
  void _tapUp(TapUpDetails details) {
    _controller.reverse();
  }

  void bottomSheet(){
    
    showMaterialModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        controller: ModalScrollController.of(context),
        child: Container(
          height: MediaQuery.of(context).size.height *0.8,
          width: MediaQuery.of(context).size.width,
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(Icons.camera_alt_outlined,color: Color(0xff002f71)),
                      Container(
                        height: 40,
                        width:  MediaQuery.of(context).size.height *0.35,
                        decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(color: Colors.grey,width: 0.5)
                        ),
                        child: Center(
                          child: TextField(
                            style: TextStyle(fontSize: 11),
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Write a Comment',
                                contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                            ),
                          ),
                        ),
                      ),

                      Icon(Icons.attach_file,color: Color(0xff002f71),),
                      Icon(Icons.emoji_emotions_rounded,color: Color(0xff002f71),),

                    ],
                  ),
                  SizedBox(height: 7)
                ],
              ),),
        ),
      ),
    );
  }
}

