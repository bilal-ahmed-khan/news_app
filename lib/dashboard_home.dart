import 'package:flutter/material.dart';
import 'package:news_app/news_feed_page.dart';
import 'package:news_app/place_holder.dart';
import 'package:news_app/profile_page.dart';

class DashboardScreens extends StatefulWidget {
  @override
  _DashboardScreensState createState() => _DashboardScreensState();
}

class _DashboardScreensState extends State<DashboardScreens> with TickerProviderStateMixin {
  TabController tabController;
  // int _currentIndex = 0;
  // final List<Widget> _children = [
  //
  //   NewsFeed(),
  //   NewsFeed(),
  //   PlaceholderWidget(Colors.deepOrange),
  //   PlaceholderWidget(Colors.green),
  //   PlaceholderWidget(Colors.pink),
  //   PlaceholderWidget(Colors.pink),
  // ];
  // void onTabTapped(int index) {
  //   setState(() {
  //     _currentIndex = index;
  //   });
  // }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    tabController = TabController(initialIndex: 0, length: 6, vsync: this);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color(0xffb91017),elevation: 10,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

            Row(
              children: [
                GestureDetector(
                    onTap: (){
                      _scaffoldKey.currentState.openDrawer();
                    },
                    child: Image(image: AssetImage('assets/icons/menu.png'),)),
                SizedBox(width: 10,),
                Image(image: AssetImage('assets/icons/search.png'),),
              ],
            ),
            Image(image: AssetImage('assets/icons/logo.png'),width: 70,),

            Row(
              children: [
                Image(image: AssetImage('assets/icons/comment.png'),),
                SizedBox(width: 10,),
                Image(image: AssetImage('assets/icons/Notification.png'),),
              ],
            ),
            // Text('sbdkbskhb')
          ],
        ),
        centerTitle: true,
      ),
      body: TabBarView(
        controller: tabController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          NewsFeed(),
          ProfilePage(),
          PlaceholderWidget(Colors.red,'3 index'),
          PlaceholderWidget(Colors.green,'4 index'),
          PlaceholderWidget(Colors.blue,'5 index'),
          PlaceholderWidget(Colors.grey,'6 index'),
          // PickImage()
        ],
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
        child: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          onTap:  (index) {
            setState(() {
//            _page = index;
              tabController.index = index;
            });
          }, // new
          // currentIndex: _currentIndex,
          backgroundColor: Color(0xff002f71),
          // currentIndex: 0, // this will be set when a new tab is tapped
          type: BottomNavigationBarType.fixed, // This
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Image(image: AssetImage('assets/icons/index1.png'),),
                title: Text('Personal')
            ),
            BottomNavigationBarItem(
                icon:  Image(image: AssetImage('assets/icons/index2.png'),),
                title: Text('Personal')
            ),
            BottomNavigationBarItem(
                icon:  Image(image: AssetImage('assets/icons/index3.png'),),
                title: Text('Personal')
            ),
            BottomNavigationBarItem(
                icon:  Image(image: AssetImage('assets/icons/index4.png'),),
                title: Text('Personal')
            ), BottomNavigationBarItem(
                icon:  Image(image: AssetImage('assets/icons/index5.png'),),
                title: Text('Personal')
            ),BottomNavigationBarItem(
                icon:  Image(image: AssetImage('assets/icons/index6.png'),),
                title: Text('Personal')
            ),

          ],

        ),
      ),
    );

  }
}
