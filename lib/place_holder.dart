import 'package:flutter/material.dart';

class PlaceholderWidget extends StatefulWidget {
  final Color color;
  final String index;

  PlaceholderWidget(this.color,this.index);

  @override
  _PlaceholderWidgetState createState() => _PlaceholderWidgetState();
}

class _PlaceholderWidgetState extends State<PlaceholderWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      width: 200.0,
      color: widget.color,
      child: Center(
        child: Text(widget.index,style: TextStyle(color: Colors.white),textScaleFactor: 1.5,),
      ),
    );
  }
}