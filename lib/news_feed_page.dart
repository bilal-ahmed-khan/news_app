import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class NewsFeed extends StatefulWidget {
  @override
  _NewsFeedState createState() => _NewsFeedState();
}

class _NewsFeedState extends State<NewsFeed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child:
          Column(
            children: [
              SizedBox(height: 2.0,)
,
              Container(
decoration: BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(5)
),
                // height: 100.0,
                width: MediaQuery.of(context).size.width ,

                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      Row(
                        children: [

                          Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/icons/profile_pic.jpg'),
                                    fit: BoxFit.cover
                                ),
                                shape: BoxShape.circle
                            ),
                            height: 35.0,
                            width: 35.0,

                          ),
                            SizedBox(width: 15,),

                            Expanded(

                              child: Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: Container(
                                  height: 35,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                      border: Border.all(color: Colors.black,width: 0.5)
                                  ),
                                  child: Center(
                                    child: TextField(
                                      style: TextStyle(fontSize: 11),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Zing, vecht, huil, bid en speek',
                                        contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )

                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 60,top: 15),
                        child: Row(
                            children: [
                              Icon(Icons.camera_enhance_outlined,size: 20,),
                              SizedBox(width: 8,),
                              Icon(Icons.emoji_emotions_outlined,size: 20,),
                              SizedBox(width: 8,),
                              Text("T",textScaleFactor: 1.24,)

                            ]),
                      )
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
decoration: BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(5)
),
                  // height: 100.0,
                  width: MediaQuery.of(context).size.width,

                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [

                            // Container(
                            //   decoration: BoxDecoration(
                            //       color: Colors.blue,
                            //       shape: BoxShape.circle
                            //   ),
                            //   height: 35.0,
                            //   width: 35.0,
                            //
                            // ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Row(
                                children: [
                                  Icon(Icons.coronavirus),
                                    SizedBox(width: 10,),
                                    Text('Nationale Propaganda Organisatie',style: TextStyle(fontWeight: FontWeight.bold),textScaleFactor: 0.8,),
                                ],
                              ),
                            ),


                            Row(
                              children: [
                                Text('3u'),
                                  SizedBox(width: 5,),
                                Icon(Icons.more_vert_rounded),
                              ],
                            ),


                          ],
                        ),
                        Divider(
                          height: 25,
                          indent: 10,
                          endIndent: 10,
                          thickness: 1,
                        ),

                        Container(
                          child: Text('Wees bang! Wij weten dat angst goed verkoopt en omdat wij onze ziel aan grote bedrijven hebben verkocht is het onze taak om jou zo bang mogelijk te maken.',textScaleFactor: 0.7),
                        ),
                        SizedBox(height: 10,),

                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.of(
                                  context)
                                  .push(
                                PageRouteBuilder(
                                  transitionDuration:
                                  Duration(milliseconds: 500),
                                  pageBuilder: (BuildContext context,
                                      Animation<double>
                                      animation,
                                      Animation<double>
                                      secondaryAnimation) {
                                    return GestureDetector(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: Image(
                                          image: AssetImage('assets/icons/back1.jpeg'),
                                          fit: BoxFit.fill,
                                        ));
                                  },
                                  transitionsBuilder: (BuildContext context,
                                      Animation<double>
                                      animation,
                                      Animation<double>
                                      secondaryAnimation,
                                      Widget
                                      child) {
                                    return Align(
                                      child:
                                      FadeTransition(
                                        opacity: animation,
                                        child: child,
                                      ),
                                    );
                                  },
                                ),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/icons/back1.jpeg'),
                                      fit: BoxFit.cover
                                  ),
                                  // shape: BoxShape.circle
                              ),
                              width: 400.0,
                              height: 200.0,

                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(children: [
                                  Image(image: AssetImage('assets/icons/Frame.png'),width: 18,),

                                Text('   99x Wappie',textScaleFactor: 0.7,style: TextStyle(
                                    color: Colors.grey
                                ),)
                              ],),
                              GestureDetector(
                                onTap: (){
                                  bottomSheet();
                                },
                                child: Row(children: [
                                    Image(image: AssetImage('assets/icons/comment.png'),width: 18,color: Color(0xff002f71),),
                                  Text('   48x Bestproken',textScaleFactor: 0.7,style: TextStyle(
                                      color: Colors.grey
                                  ),)
                                ],),
                              ),
                              Row(children: [
                                  Image(image: AssetImage('assets/icons/share.png'),width: 18,),
                                Text('   48x Geedeeid',textScaleFactor: 0.7,style: TextStyle(
                                    color: Colors.grey
                                ),)
                              ],),
                            ],
                          ),
                        ),
                        // Row(
                        //   children: [
                        //
                        //     Container(
                        //       decoration: BoxDecoration(
                        //           image: DecorationImage(
                        //               image: AssetImage('assets/icons/profile_pic.jpg'),
                        //               fit: BoxFit.cover
                        //           ),
                        //           shape: BoxShape.circle
                        //       ),
                        //       height: 35.0,
                        //       width: 35.0,
                        //
                        //     ),
                        //     SizedBox(width: 15,),
                        //
                        //     Expanded(
                        //
                        //       child: Container(
                        //         height: 35,
                        //         decoration: BoxDecoration(
                        //             borderRadius: BorderRadius.circular(25),
                        //             border: Border.all(color: Colors.black,width: 0.5)
                        //         ),
                        //         child: Center(
                        //           child: TextField(
                        //             style: TextStyle(fontSize: 11),
                        //             decoration: InputDecoration(
                        //                 border: InputBorder.none,
                        //                 hintText: 'Zing, vecht, huil, bid en speek',
                        //
                        //                 contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //     )
                        //
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
decoration: BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(5)
),
                  // height: 100.0,
                  width: MediaQuery.of(context).size.width,

                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [

                            // Container(
                            //   decoration: BoxDecoration(
                            //       color: Colors.blue,
                            //       shape: BoxShape.circle
                            //   ),
                            //   height: 35.0,
                            //   width: 35.0,
                            //
                            // ),
                            Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage('assets/icons/profile_pic.jpg'),
                                        fit: BoxFit.cover
                                      ),
                                      shape: BoxShape.circle
                                  ),
                                  height: 35.0,
                                  width: 35.0,

                                ),
                                  SizedBox(width: 5,),
                                  Text('René Mirck',style: TextStyle(fontWeight: FontWeight.bold),textScaleFactor: 0.8,),
                                Icon(Icons.play_arrow,size: 16,color: Colors.grey,),
                                Text('Waarheidzoekers',style: TextStyle(color: Color(0xff002f71)),textScaleFactor: 0.8,),
                              ],
                            ),Row(
                              children: [
                                Text('3u'),
                                  SizedBox(width: 5,),
                                Icon(Icons.more_vert_rounded),
                              ],
                            ),


                          ],
                        ),
                        Divider(
                          height: 25,
                          indent: 10,
                          endIndent: 10,
                          thickness: 1,
                        ),

                        Container(
                          child: Text('Wat vind jij d’r nu van Kokki? Heb ik het een beetje goed uitgelegd zo? Nee Peppi, je moet zeggen dat het vaccin een frikandel is. Dan snappen ze het wel.',textScaleFactor: 0.7),
                        ),
                        SizedBox(height: 10,),

                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.of(
                                  context)
                                  .push(
                                PageRouteBuilder(
                                  transitionDuration:
                                  Duration(milliseconds: 500),
                                  pageBuilder: (BuildContext context,
                                      Animation<double>
                                      animation,
                                      Animation<double>
                                      secondaryAnimation) {
                                    return GestureDetector(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: Image(
                                          image: AssetImage('assets/icons/back2.jpeg'),
                                          fit: BoxFit.fill,
                                        ));
                                  },
                                  transitionsBuilder: (BuildContext context,
                                      Animation<double>
                                      animation,
                                      Animation<double>
                                      secondaryAnimation,
                                      Widget
                                      child) {
                                    return Align(
                                      child:
                                      FadeTransition(
                                        opacity: animation,
                                        child: child,
                                      ),
                                    );
                                  },
                                ),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/icons/back2.jpeg'),
                                    fit: BoxFit.cover
                                ),
                                // shape: BoxShape.circle
                              ),
                              width: 400.0,
                              height: 200.0,

                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(children: [
                                  Image(image: AssetImage('assets/icons/Frame.png'),width: 18,),

                                Text(' 99x Wappie',textScaleFactor: 0.8,style: TextStyle(
                                    color: Colors.grey
                                ),)
                              ],),
                              GestureDetector(
                                onTap: (){
                                  bottomSheet();
                                },
                                child: Row(children: [
                                    Image(image: AssetImage('assets/icons/comment.png'),width: 18,),
                                  Text(' 48x Bestproken',textScaleFactor: 0.8,
                                    style: TextStyle(
                                    color: Colors.grey
                                  ),
                                  )
                                ],),
                              ),
                              Row(children: [
                                  Image(image: AssetImage('assets/icons/share.png'),width: 18,),
                                Text(' 48x Geedeeid',textScaleFactor: 0.8,style: TextStyle(
                                    color: Colors.grey
                                ),)
                              ],),
                            ],
                          ),
                        ),
                        // Row(
                        //   children: [
                        //
                        //     Container(
                        //       decoration: BoxDecoration(
                        //           image: DecorationImage(
                        //               image: AssetImage('assets/icons/profile_pic.jpg'),
                        //               fit: BoxFit.cover
                        //           ),
                        //           shape: BoxShape.circle
                        //       ),
                        //       height: 35.0,
                        //       width: 35.0,
                        //
                        //     ),
                        //     SizedBox(width: 15,),
                        //
                        //     Expanded(
                        //
                        //       child: Container(
                        //         height: 35,
                        //         decoration: BoxDecoration(
                        //             borderRadius: BorderRadius.circular(25),
                        //             border: Border.all(color: Colors.black,width: 0.5)
                        //         ),
                        //         child: Center(
                        //           child: TextField(
                        //             style: TextStyle(fontSize: 11),
                        //             decoration: InputDecoration(
                        //                 border: InputBorder.none,
                        //                 hintText: 'Zing, vecht, huil, bid en speek',
                        //
                        //                 contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //     )
                        //
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                ),
              ),


          SizedBox(height: 25,),

            ],
          ),
        ),
      ),
    );
  }
  void bottomSheet(){

    showMaterialModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        controller: ModalScrollController.of(context),
        child: Container(
          height: MediaQuery.of(context).size.height *0.8,
          width: MediaQuery.of(context).size.width,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(Icons.camera_alt_outlined,color: Color(0xff002f71)),
                    Container(
                      height: 40,
                      width:  MediaQuery.of(context).size.height *0.35,
                      decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(color: Colors.grey,width: 0.5)
                      ),
                      child: Center(
                        child: TextField(
                          style: TextStyle(fontSize: 11),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Write a Comment',
                              contentPadding: EdgeInsets.only(left: 15,bottom: 12)
                          ),
                        ),
                      ),
                    ),

                    Icon(Icons.attach_file,color: Color(0xff002f71),),
                    Icon(Icons.emoji_emotions_rounded,color: Color(0xff002f71),),

                  ],
                ),
                SizedBox(height: 7)
              ],
            ),),
        ),
      ),
    );
  }
}
